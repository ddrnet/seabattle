﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace SeaB
{
    /// <summary>
    /// Игровая доска
    /// </summary>
    public class Fields
    {
        /// <summary>
        /// Коллекция игровых полей
        /// </summary>
        public Field[] _Collection;
        /// <summary>
        /// Коллекция навигационных букв (декорация)
        /// </summary>
        public Label[] _Match;
        /// <summary>
        /// Создание новой игровой доски
        /// </summary>
        /// <param name="StartLoc">Начальная позиция</param>
        /// <param name="size">Размер поля</param>
        public Fields(Point StartLoc, int size)
        {
            _Collection = new Field[100];
            _Match = new Label[20];
            int M = 0;
            for (int X = 0; X < 10; X++ )
            {
                for (int Y = 0; Y < 10; Y++)
                {
                    _Collection[X*10+Y] = new Field(
                        (X * 10 + Y),
                        new Point(StartLoc.X + size * X, StartLoc.Y + size * Y),
                        new Point(X, Y), 
                        size);
                    if (X == 0)//Пишем цифры
                    {
                        _Match[M] = new Label();
                        _Match[M].AutoSize = true;
                        _Match[M].Size = new System.Drawing.Size(size, size);
                        _Match[M].Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                        _Match[M].Location = new Point(StartLoc.X - size*5/4,
                                                       StartLoc.Y + Y * size);
                        switch (Y)
                        {
                            case 0:
                                {
                                    _Match[M].Text = " 1";
                                    break;
                                }
                            case 1:
                                {
                                    _Match[M].Text = " 2";
                                    break;
                                }
                            case 2:
                                {
                                    _Match[M].Text = " 3";
                                    break;
                                }
                            case 3:
                                {
                                    _Match[M].Text = " 4";
                                    break;
                                }
                            case 4:
                                {
                                    _Match[M].Text = " 5";
                                    break;
                                }
                            case 5:
                                {
                                    _Match[M].Text = " 6";
                                    break;
                                }
                            case 6:
                                {
                                    _Match[M].Text = " 7";
                                    break;
                                }
                            case 7:
                                {
                                    _Match[M].Text = " 8";
                                    break;
                                }
                            case 8:
                                {
                                    _Match[M].Text = " 9";
                                    break;
                                }
                            case 9:
                                {
                                    _Match[M].Text = "10";
                                    break;
                                }
                        }
                        M++;
                    }
                    if (Y == 0)//Пишем буквы
                    {
                        _Match[M] = new Label();
                        _Match[M].AutoSize = true;
                        _Match[M].Size = new System.Drawing.Size(size, size);
                        _Match[M].Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                        _Match[M].Location = new Point(StartLoc.X + size * X,
                                                       StartLoc.Y - 5 / 4 * size);
                        switch (X)
                        {
                            case 0:
                                {
                                    _Match[M].Text = "А";
                                    break;
                                }
                            case 1:
                                {
                                    _Match[M].Text = "Б";
                                    break;
                                }
                            case 2:
                                {
                                    _Match[M].Text = "В";
                                    break;
                                }
                            case 3:
                                {
                                    _Match[M].Text = "Г";
                                    break;
                                }
                            case 4:
                                {
                                    _Match[M].Text = "Д";
                                    break;
                                }
                            case 5:
                                {
                                    _Match[M].Text = "Е";
                                    break;
                                }
                            case 6:
                                {
                                    _Match[M].Text = "Ж";
                                    break;
                                }
                            case 7:
                                {
                                    _Match[M].Text = "З";
                                    break;
                                }
                            case 8:
                                {
                                    _Match[M].Text = "И";
                                    break;
                                }
                            case 9:
                                {
                                    _Match[M].Text = "К";
                                    break;
                                }
                        }
                        M++;
                    }
                }
            }
        }
   
    }
}
