﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SeaB
{
    /// <summary>
    /// Корабли
    /// </summary>
    public class Ships
    {
        /// <summary>
        /// Коллекция кораблей
        /// </summary>
        public Ship[] _Collection;
        private int _ShipsCout;
        public int ShipsCount
        {
            get { return _ShipsCout; }
        }
        public Ships()
        {
            _ShipsCout = 0;
            _Collection = new Ship[10];
            for (int i = 0; i < 10; i++) _Collection[i] = new Ship(new Point(-2, -2), 1);
        }
        /// <summary>
        /// Пытается добавить корабль и сообщает результат
        /// </summary>
        /// <param name="ship">Корабль для добавления</param>
        public bool Add(Ship ship)
        {
            if (ShipsCount < 10 && !_ChekCollisoin(ship))
            {
                _Collection[ShipsCount] = ship;
                _ShipsCout++;
                return true;
            }
            return false;
        }
        private bool _ChekCollisoin(Ship ship)
        {
           //Проверяем что корабль не выходит за границы игрового поля
           if ((from t in ship.Coords where t.X > 9 || t.Y > 9 select t).Count() > 0) return true;

            if (_Collection[0] != null)
            {
                //Добавить на проверку относительно других кораблей
                var CordLists = from t in _Collection select t.Coords;
                List<Point> Cords = new List<Point>();
                foreach (var L in CordLists) Cords.AddRange(L);
                foreach (Point ShipPart in ship.Coords)
                {
                    if ((from t in Cords
                         where
         (ShipPart.X <= t.X + 1 && ShipPart.X >= t.X - 1) && (ShipPart.Y <= t.Y + 1 && ShipPart.Y >= t.Y - 1)
                         select t).Count() > 0) return true;
                }
            }
            return false;
        }
    }
}
