﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace SeaB
{
    /// <summary>
    /// Игровое поле
    /// </summary>
    public class Field : Button, IDisposable
    {
        private int _id;
        public int id { get { return _id; } set { _id = value; } }
        private byte _state = 3;
        /// <summary>
        /// Сотояние поле (0-промах,1-ранили,2-корабль,3-пусто)
        /// </summary>
        public int state
        {
            get { return _state; }
            set
            {
                if (value == 0)
                {
                    ToShot();
                    _state = 0;
                }
                else if (value == 1)
                {
                    ToRed();
                    _state = 1;
                }
                else if (value == 2)
                {
                    ToGreen();
                    _state = 2;
                }
                else
                {
                    ToBlue();
                    _state = 3;
                }
            }
        }
        public Point GameLoc;
        /// <summary>
        /// Создание пустой ячейки
        /// </summary>
        /// <param name="i">ID ячейки</param>
        /// <param name="Loc">Координаты на форме</param>
        /// <param name="GLoc">Координаты игровые</param>
        public Field(int i, Point Loc, Point GLoc, int size) : base()
        {
            Location = Loc;
            GameLoc = GLoc;
            id = i;
            Font = new System.Drawing.Font("Microsoft Sans Seri", 15.75f);
            FlatStyle = FlatStyle.Flat;
            Size = new System.Drawing.Size(size, size);
            BackColor = System.Drawing.SystemColors.Highlight;
            Margin = new System.Windows.Forms.Padding(0);
            FlatAppearance.BorderSize = 1;
            Text = " ";
        }
        /// <summary>
        /// Сделать пустой голубой клеткой
        /// </summary>
        public void ToBlue()
        {
            BackColor = System.Drawing.SystemColors.Highlight;
            Text = " ";
        }
        /// <summary>
        /// Сделать пустой красной клеткой
        /// </summary>
        public void ToRed()
        {
            BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            Text = " ";
        }
        /// <summary>
        /// Сделать пустой зеленой клеткой
        /// </summary>
        public void ToGreen()
        {
            BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            Text = " ";
        }
        /// <summary>
        /// Сделать клеткой промоха
        /// </summary>
        public void ToShot()
        {
            BackColor = System.Drawing.SystemColors.Highlight;
            Text = "◉";
        }
       
    }
}
