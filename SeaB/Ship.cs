﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace SeaB
{
    /// <summary>
    /// Логический объект корабля
    /// </summary>
    public class Ship
    {
        /// <summary>
        /// Набор игровых координат
        /// </summary>
        public List<Point> Coords;
        /// <summary>
        /// Колличество палуб
        /// </summary>
        public int Size
        {
            get { return Coords.Count; }
        }
        /// <summary>
        /// Максимально возможный объем жизней
        /// </summary>
        public int maxHP { get { return Size; } }
        int _HP;
        /// <summary>
        /// Колличество жизней. 
        /// Можно добавить или отнять
        /// </summary>
        public int HP
        {
            get { return _HP; }
            set
            {
                if (value + _HP > maxHP) _HP = maxHP;
                else if (value + _HP < 0) _HP = 0;
                else _HP += value;
            }
        }
        /// <summary>
        /// Новый корабль
        /// </summary>
        /// <param name="StartPos">Координата начала</param>
        /// <param name="size">Размер</param>
        /// <param name="Horizontal">Параметр отвечащий за горизонтальное положение</param>
        public Ship(Point StartPos,int size, bool Horizontal = true)
        {
           Coords = new List<Point>();
           if (Horizontal)
            {
                for (int i = 0; i < size; i++)
                {
                    Coords.Add(new Point(StartPos.X + i, StartPos.Y));
                }
            }
           else
            {
                for (int i = 0; i < size; i++)
                {
                    Coords.Add(new Point(StartPos.X, StartPos.Y+i));
                }
            }
            _HP = size; 
        }
    }
}
