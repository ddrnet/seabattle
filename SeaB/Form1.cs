﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SeaB
{
    public partial class Form1 : Form 
    {
        public int towin = 0;
        public int tolose = 0;
        public Fields PlayerDesk;
        public Fields AIDesk;
        public Ships OurShips;
        public AI Killer = new AI();
        public Form1()
        { 
            InitializeComponent();
            PlayerDesk = new Fields(new Point(75, 100), 35);
            toolStripStatusLabel1.Text = "Ожидание начала новой игры";
            splitContainer1.Panel1.Controls.AddRange(PlayerDesk._Match);    
            splitContainer1.Panel1.Controls.AddRange(PlayerDesk._Collection);
            AIDesk = new Fields(new Point(75, 100), 35);
            splitContainer1.Panel2.Controls.AddRange(AIDesk._Match);
            splitContainer1.Panel2.Controls.AddRange(AIDesk._Collection);
            ShipsOnDesk = 0;
            OurShips = new Ships();
            foreach (Field F in PlayerDesk._Collection) F.MouseUp += new MouseEventHandler(AddShip);
            toolStripStatusLabel1.Text = "Расстановка кораблей: 4х палубный (Правый клик - вертикально, левый клик - горизонтально)";
            сделатьПервыйХодToolStripMenuItem.Enabled = false;

        }
   
        /// <summary>
        /// Нажатия по нашей игровой доске с активацией игрвой логики
        /// </summary>
        /// <param name="sender">Нажатая ячейка</param>
        /// <param name="e">Аргумент мыши</param>
        private void OponentClik(object sender, MouseEventArgs e)
        {
           
            if (((Field)sender).state == 3)
            {
                ((Field)sender).state = 0;
                //Вызов AI для отвтеного удара
                Point tokill = Killer.Shoot();
                OurClik((from t in AIDesk._Collection where t.GameLoc == tokill select t).Last(), new EventArgs());
            }
            else if(((Field)sender).state == 2)
            {
                ((Field)sender).state = 1;
                Point isdead = new Point(-10,-10);
                foreach (Ship ship in OurShips._Collection)
                {
                    foreach (Point cor in ship.Coords)
                    {
                        if (cor == ((Field)sender).GameLoc)
                        {
                            ship.HP = -1;
                            if (ship.HP == 0)
                            {
                                isdead = ship.Coords.First();
                                tolose++;
                            }
                        }
                    }
                    foreach (Point cor in ship.Coords)
                    {//Закрасить точки если корабль убили
                        if (isdead == ship.Coords.First())
                        {
                            {
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X + 1) * 10 + cor.Y].state == 3)
                                        PlayerDesk._Collection[(cor.X + 1) * 10 + cor.Y].state = 0;
                                }
                                catch { }
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X - 1) * 10 + cor.Y].state == 3)
                                        PlayerDesk._Collection[(cor.X - 1) * 10 + cor.Y].state = 0;
                                }
                                catch { }
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X + 1) * 10 + cor.Y + 1].state == 3 && cor.Y + 1 < 10)
                                        PlayerDesk._Collection[(cor.X + 1) * 10 + cor.Y + 1].state = 0;
                                }
                                catch { }
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X + 0) * 10 + cor.Y + 1].state == 3 && cor.Y + 1 < 10)
                                        PlayerDesk._Collection[(cor.X + 0) * 10 + cor.Y + 1].state = 0;
                                }
                                catch { }
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X - 1) * 10 + cor.Y + 1].state == 3 && cor.Y + 1 < 10)
                                        PlayerDesk._Collection[(cor.X - 1) * 10 + cor.Y + 1].state = 0;
                                }
                                catch { }
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X + 1) * 10 + cor.Y - 1].state == 3 && cor.Y - 1 > -1)
                                        PlayerDesk._Collection[(cor.X + 1) * 10 + cor.Y - 1].state = 0;
                                }
                                catch { }
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X + 0) * 10 + cor.Y - 1].state == 3 && cor.Y - 1 > -1)
                                        PlayerDesk._Collection[(cor.X + 0) * 10 + cor.Y - 1].state = 0;
                                }
                                catch { }
                                try
                                {
                                    if (PlayerDesk._Collection[(cor.X - 1) * 10 + cor.Y - 1].state == 3 && cor.Y - 1 > -1)
                                        PlayerDesk._Collection[(cor.X - 1) * 10 + cor.Y - 1].state = 0;
                                }
                                catch { }
                            }
                        }
                    }
                    if (isdead != new Point(-10,-10))
                    {
                        break;
                    }

                }
                
            }
            toolStripStatusLabel1.Text = "Ход противника";
            //Проверить условия поражения
            if (tolose == 20)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                player.SoundLocation = "Lose8.wav";
                player.Play();
                MessageBox.Show(
                  "Поражение!",
                  "Результат матча",
                  MessageBoxButtons.OK,
                  MessageBoxIcon.Information);
                toolStripStatusLabel1.Text = "Игра окончена";
                новаяИграToolStripMenuItem_Click(null, new EventArgs());
            }
        }
        /// <summary>
        /// Нажитие AI по доске противника
        /// </summary>
        /// <param name="sender">Нажатая ячейка</param>
        /// <param name="e">Аргумент мыши</param>
        private void OurClik(object sender, EventArgs e)
        {
            
            string word = "";
            switch (((Field)sender).GameLoc.X)
            {
                case 0:
                    {
                        word = "А";
                        break;
                    }
                case 1:
                    {
                        word = "Б";
                        break;
                    }
                case 2:
                    {
                        word = "В";
                        break;
                    }
                case 3:
                    {
                        word = "Г";
                        break;
                    }
                case 4:
                    {
                        word = "Д";
                        break;
                    }
                case 5:
                    {
                        word = "Е";
                        break;
                    }
                case 6:
                    {
                        word = "Ж";
                        break;
                    }
                case 7:
                    {
                        word = "З";
                        break;
                    }
                case 8:
                    {
                        word = "И";
                        break;
                    }
                case 9:
                    {
                        word = "К";
                        break;
                    }
            }
            word = string.Format("{0}{1} - есть попадание?", word, ((Field)sender).GameLoc.Y+1);
            DialogResult result = MessageBox.Show(
              word,
              "Результат стрельбы",
              MessageBoxButtons.YesNo,
              MessageBoxIcon.Question,
              MessageBoxDefaultButton.Button1);
            if (result == DialogResult.Yes)
            {
                towin++;
                result = MessageBox.Show(
                  "Противник потоплен?",
                  "Результат попадания",
                  MessageBoxButtons.YesNo,
                  MessageBoxIcon.Question,
                  MessageBoxDefaultButton.Button1);
                if (result == DialogResult.Yes)
                {
                    ((Field)sender).state = 1;
                    //вызов AI и закраска поля вокруг
                    List<Point> Area = AreaShip(((Field)sender), AIDesk._Collection);
                    foreach (Point cor in Area)
                        {
                        try
                        {
                            if (AIDesk._Collection[(cor.X + 1) * 10 + cor.Y].state == 3)
                                AIDesk._Collection[(cor.X + 1) * 10 + cor.Y].state = 0;
                        }
                        catch { }
                        try
                        {
                            if (AIDesk._Collection[(cor.X - 1) * 10 + cor.Y].state == 3)
                                AIDesk._Collection[(cor.X - 1) * 10 + cor.Y].state = 0;
                        }
                        catch { }
                        try
                        {

                            if (AIDesk._Collection[(cor.X + 1) * 10 + cor.Y + 1].state == 3 && cor.Y + 1 < 10)
                                AIDesk._Collection[(cor.X + 1) * 10 + cor.Y + 1].state = 0;
                        }
                        catch { }
                        try
                        {
                            if (AIDesk._Collection[(cor.X + 0) * 10 + cor.Y + 1].state == 3 && cor.Y + 1 < 10)
                                AIDesk._Collection[(cor.X + 0) * 10 + cor.Y + 1].state = 0;
                        }
                        catch { }
                        try
                        {
                            if (AIDesk._Collection[(cor.X - 1) * 10 + cor.Y + 1].state == 3 && cor.Y + 1 < 10)
                                AIDesk._Collection[(cor.X - 1) * 10 + cor.Y + 1].state = 0;
                        }
                        catch { }
                        try
                        {
                            if (AIDesk._Collection[(cor.X + 1) * 10 + cor.Y - 1].state == 3 && cor.Y - 1 > -1)
                                AIDesk._Collection[(cor.X + 1) * 10 + cor.Y - 1].state = 0;
                        }
                        catch { }
                        try
                        {
                            if (AIDesk._Collection[(cor.X + 0) * 10 + cor.Y - 1].state == 3 && cor.Y - 1 > -1)
                                AIDesk._Collection[(cor.X + 0) * 10 + cor.Y - 1].state = 0;
                        }
                        catch { }
                        try
                        {
                            if (AIDesk._Collection[(cor.X - 1) * 10 + cor.Y - 1].state == 3 && cor.Y - 1 > -1)
                                AIDesk._Collection[(cor.X - 1) * 10 + cor.Y - 1].state = 0;
                        }
                        catch { }
                    }
                    if (towin == 20)
                    {
                        System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                        player.SoundLocation = "Win.wav";
                        player.Play();
                        MessageBox.Show(
                          "Победа!",
                          "Результат матча",
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
                        toolStripStatusLabel1.Text = "Игра окончена";
                        новаяИграToolStripMenuItem_Click(null, new EventArgs());
                    }
                    else
                    {
                        Point tokill = Killer.Shoot(2);
                        OurClik((from t in AIDesk._Collection where t.GameLoc == tokill select t).Last(), new EventArgs());
                    }
                }
                else
                {
                    ((Field)sender).state = 1;
                    //вызов AI
                    Point tokill = Killer.Shoot(1);
                    OurClik((from t in AIDesk._Collection where t.GameLoc == tokill select t).Last(), new EventArgs());
                }
            }
            else ((Field)sender).state = 0;
        }
        /// <summary>
        /// Обходит корабль противника и возвращает координаты его частей
        /// </summary>
        /// <param name="St">Одна из ячеек корабля</param>
        /// <param name="Col">Игровое поле</param>
        /// <returns></returns>
        public List<Point> AreaShip(Field St, Field[] Col)
        {
            List<Point> Answ = new List<Point>();
            Answ.Add(St.GameLoc);
            Point search = St.GameLoc;
            if (!((search.X + 1) * 10 + search.Y < 0) && !((search.X + 1) * 10 + search.Y > 99))
            {
                while (Col[(search.X + 1) * 10 + search.Y].state == 1)
                {
                    search = new Point(search.X + 1, search.Y);
                    Answ.Add(search);
                    if ((search.X + 1) * 10 + search.Y < 0 || (search.X + 1) * 10 + search.Y > 99) break;
                }
            }
            search = St.GameLoc;
            if (!((search.X - 1) * 10 + search.Y < 0) && !((search.X - 1) * 10 + search.Y > 99))
            {
                while (Col[(search.X - 1) * 10 + search.Y].state == 1)
                {
                    search = new Point(search.X - 1, search.Y);
                    Answ.Add(search);
                    if ((search.X - 1) * 10 + search.Y < 0 || (search.X - 1) * 10 + search.Y > 99)
                    {
                        break; 

                    }
                }
            }
            search = St.GameLoc;
            if (!((search.X) * 10 + search.Y + 1 < 0) && !((search.X) * 10 + search.Y + 1 > 99))
            {
                while (Col[(search.X) * 10 + search.Y + 1].state == 1 && search.Y + 1 < 10)
                {
                    search = new Point(search.X, search.Y + 1);
                    Answ.Add(search);
                    if ((search.X) * 10 + search.Y + 1 < 0 || (search.X) * 10 + search.Y + 1 > 99) break;
                }
            }
            search = St.GameLoc;
            if (!((search.X) * 10 + search.Y - 1 < 0 )&& !((search.X) * 10 + search.Y - 1 > 99))
            {
                while (Col[(search.X) * 10 + search.Y - 1].state == 1 && search.Y - 1 >= 0)
                {
                    search = new Point(search.X, search.Y - 1);
                    Answ.Add(search);
                    if ((search.X) * 10 + search.Y - 1 < 0 || (search.X) * 10 + search.Y - 1 > 99) break;           
                }
            }
            return Answ;
        }
        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private int ShipsOnDesk;
        /// <summary>
        /// С ручной расстановкой кораблей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void новаяИграToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SuspendLayout();
            foreach(Field ToRem in PlayerDesk._Collection)
            {
                splitContainer1.Panel1.Controls.Remove(ToRem);
            }
            foreach (Label ToRem in PlayerDesk._Match)
            {
                splitContainer1.Panel1.Controls.Remove(ToRem);
            }
            foreach (Field ToRem in AIDesk._Collection)
            {
                splitContainer1.Panel2.Controls.Remove(ToRem);
            }
            foreach (Label ToRem in AIDesk._Match)
            {
                splitContainer1.Panel2.Controls.Remove(ToRem);
            }
            tolose = 0;
            towin = 0;
            PlayerDesk = new Fields(new Point(75, 100), 35);
            
           

            splitContainer1.Panel1.Controls.AddRange(PlayerDesk._Match);
            splitContainer1.Panel1.Controls.AddRange(PlayerDesk._Collection);
            AIDesk = new Fields(new Point(75, 100), 35);
      
         
           
            splitContainer1.Panel2.Controls.AddRange(AIDesk._Match);
            splitContainer1.Panel2.Controls.AddRange(AIDesk._Collection);
            ShipsOnDesk = 0;
            OurShips = new Ships();
            foreach (Field F in PlayerDesk._Collection) F.MouseUp += new MouseEventHandler(AddShip);
            toolStripStatusLabel1.Text = "Расстановка кораблей: 4х палубный (Правый клик - вертикально, левый клик - горизонтально)";
            сделатьПервыйХодToolStripMenuItem.Enabled = false;
            Killer = new AI();
            расставитьКораблиToolStripMenuItem.Enabled = true;
            this.ResumeLayout();
        }
        ToolTip t = new ToolTip();
        private void AddShip(object sender, MouseEventArgs e)
        {
            расставитьКораблиToolStripMenuItem.Enabled = false;
            t.RemoveAll();
            bool Add = true;
            int len = 0;
            switch (ShipsOnDesk)
            {
                case 0:
                    {
                        if (e.Button == MouseButtons.Right)
                        {
                           Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 4, false));
                           
                        }
                        else
                        {
                            Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 4));
                        }
                        len = 4;
                        break;
                    }
                case 1:
                case 2:
                    {
                        if (e.Button == MouseButtons.Right)
                        {
                            Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 3, false));
                        }
                        else
                        {
                            Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 3));
                        }
                        len = 3;
                        break;
                    }
                case 3:
                case 4:
                case 5:
                    {
                        if (e.Button == MouseButtons.Right)
                        {
                            Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 2, false));
                        }
                        else
                        {
                            Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 2));
                        }
                        this.Focus();
                        len = 2;
                        break;
                    }
                case 6:
                case 7:
                case 8:
                case 9:
                    {
                        if (e.Button == MouseButtons.Right)
                        {
                            Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 1, false));
                        }
                        else
                        {
                            Add = OurShips.Add(new Ship(((Field)sender).GameLoc, 1));
                        }
                        len = 1;
                        break;
                    }
            }
            if (Add)
            {
                if (e.Button == MouseButtons.Right)
                {
                    for (int i = 0; i < len; i++)
                    {
                        PlayerDesk._Collection[((Field)sender).id + i].state = 2;
                    }
                }
                else
                {
                    for (int i = 0; i < len; i++)
                    {
                        PlayerDesk._Collection[((Field)sender).id + i*10].state = 2;
                    }
                }
                ShipsOnDesk++;
            }
            else
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer();

                player.SoundLocation = "ShipErr.wav";
                player.Play();
                t.ToolTipTitle = "Ошибка расположения";
                t.ToolTipIcon = ToolTipIcon.Error;
                t.Show("Корабль не может быть расположе в этой клетке в данной ориентации.", ((Field)sender));
            }
            if (ShipsOnDesk >= 1 && ShipsOnDesk < 3) toolStripStatusLabel1.Text = "Расстановка кораблей: 3х палубный (Правый клик - вертикально, левый клик - горизонтально)";
            else if (ShipsOnDesk >= 3 && ShipsOnDesk < 6) toolStripStatusLabel1.Text = "Расстановка кораблей: 2х палубный (Правый клик - вертикально, левый клик - горизонтально)";
            else toolStripStatusLabel1.Text = "Расстановка кораблей: 1но палубный";
            if (ShipsOnDesk >= 10)
            {
                сделатьПервыйХодToolStripMenuItem.Enabled = true;
                toolStripStatusLabel1.Text = "Корабли размещены, ожидание первого хода";
               
                foreach (Field F in PlayerDesk._Collection) F.MouseUp -= new MouseEventHandler(AddShip);
                foreach (Field F in PlayerDesk._Collection) F.MouseUp += new MouseEventHandler(OponentClik);
                DialogResult result = MessageBox.Show(
                  "Передать первый ход оппоненту?",
                  "Первый ход",
                  MessageBoxButtons.YesNo,
                  MessageBoxIcon.Question,
                  MessageBoxDefaultButton.Button1);
                if (result == DialogResult.No) { toolStripStatusLabel1.Text = "Ход противника"; сделатьПервыйХодToolStripMenuItem_Click(null, new EventArgs()); }

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(
              "Вы покидаете нас, Адмирал?",
              "Завершение игры",
              MessageBoxButtons.YesNo,
              MessageBoxIcon.Question,
              MessageBoxDefaultButton.Button1);
            if (result == DialogResult.Yes) e.Cancel = false;
            else e.Cancel = true;
        }

        private void сделатьПервыйХодToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Point tokill = Killer.Shoot();
            OurClik((from t in AIDesk._Collection where t.GameLoc == tokill select t).Last(), new EventArgs());
        }
        /// <summary>
        /// Програмно тыкаем куда надо как в ручном варианте
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void расставитьКораблиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddShip(PlayerDesk._Collection[6], new MouseEventArgs(MouseButtons.Right, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[80], new MouseEventArgs(MouseButtons.Right, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[87], new MouseEventArgs(MouseButtons.Right, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[94], new MouseEventArgs(MouseButtons.Right, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[11], new MouseEventArgs(MouseButtons.Right, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[14], new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[28], new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[40], new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[54], new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
            AddShip(PlayerDesk._Collection[62], new MouseEventArgs(MouseButtons.Left, 1, 0, 0, 0));
            расставитьКораблиToolStripMenuItem.Enabled = false;
        }

        private void авторыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About frm = new About();
            frm.ShowDialog();
        }
    }
}
