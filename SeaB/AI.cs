﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SeaB
{
    //промах=0 попал=1 убил=2
    public class AI
    {
        List<Point> _hit;
        List<Point> _shooted;
        Random _randomizer;
        public AI()
        {
            _randomizer = new Random();
            _shooted = new List<Point>();
            _hit = new List<Point>();
        }
        private Point ShootHit(int status)
        {
            Point toRet = new Point();
            if (status != 0)
            {
                _hit.Add(_shooted[_shooted.Count - 1]);
            }
            if (_hit.Count == 1)
            {
                if (_hit[0].X - 1 >= 0 && !_shooted.Contains(new Point(_hit[0].X - 1, _hit[0].Y)))
                {
                    toRet = new Point(_hit[0].X - 1, _hit[0].Y);
                    _shooted.Add(toRet);
                    Point _arrP = toRet;
                    return _arrP;
                }
                else if (_hit[0].Y - 1 >= 0 && !_shooted.Contains(new Point(_hit[0].X, _hit[0].Y - 1)))
                {
                    toRet = new Point(_hit[0].X, _hit[0].Y - 1);
                    _shooted.Add(toRet);
                    Point _arrP = toRet;
                    return _arrP;
                }
                else if (_hit[0].X + 1 < 10 && !_shooted.Contains(new Point(_hit[0].X + 1, _hit[0].Y)))
                {
                    toRet = new Point(_hit[0].X + 1, _hit[0].Y);
                    _shooted.Add(toRet);
                    Point _arrP = toRet;
                    return _arrP;
                }
                else //if (_hit[0].Y + 1 < 10 && !_shooted.Contains(new Point(_hit[0].X, _hit[0].Y + 1)))
                {
                    toRet = new Point(_hit[0].X, _hit[0].Y + 1);
                    _shooted.Add(toRet);
                    Point _arrP = toRet;
                    return _arrP;
                }
            }
            else
            {
                if (_hit[0].X == _hit[1].X)
                {
                    if (_hit.Min(point => point.Y) - 1 >= 0 && !_shooted.Contains(new Point(_hit[0].X, _hit.Min(point => point.Y) - 1)))
                    {
                        toRet = new Point(_hit[0].X, _hit.Min(point => point.Y) - 1);
                        _shooted.Add(toRet);
                        Point _arrP = toRet;
                        return _arrP;
                    }
                    else
                    {
                        toRet = new Point(_hit[0].X, _hit.Max(point => point.Y) + 1);
                        _shooted.Add(toRet);
                        Point _arrP = toRet;
                        return _arrP;
                    }
                }
                else
                {
                    if (_hit.Min(point => point.X) - 1 >= 0 && !_shooted.Contains(new Point(_hit.Min(point => point.X) - 1, _hit[0].Y)))
                    {
                        toRet = new Point(_hit.Min(point => point.X) - 1, _hit[0].Y);
                        _shooted.Add(toRet);
                        Point _arrP = toRet;
                        return _arrP;
                    }
                    else
                    {
                        toRet = new Point(_hit.Max(point => point.X) + 1, _hit[0].Y);
                        _shooted.Add(toRet);
                        Point _arrP = toRet;
                        return _arrP;
                    }
                }
            }
        }

        private bool turn = true;
        private bool patr1 = true; private bool patr2 = true;
        private Point newShoot(int status)
        {
            Point toRet = new Point();
            int[] order_shoot1 = { 1, 0, 3, 2, 1, 0, 3, 2, 1, 0 };
            int[] order_shoot2 = { 3, 2, 1, 0, 3, 2, 1, 0, 3, 2 };
            if (_hit.Count == 0)
            {
                if (patr1)
                    if (turn)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            for (int j = order_shoot2[i]; j < 10; j += 4)
                            {
                                if (!_shooted.Contains(new Point(j, i)))
                                {
                                    toRet = new Point(j, i);
                                    _shooted.Add(toRet);
                                    turn = !turn;
                                    return toRet;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 9; i >= 0; i--)
                        {
                            for (int j = order_shoot2[i]; j < 10; j += 4)
                            {
                                if (!_shooted.Contains(new Point(j, i)))
                                {
                                    toRet = new Point(j, i);
                                    _shooted.Add(toRet);
                                    turn = !turn;
                                    return toRet;
                                }
                            }
                        }
                    }
                if (turn)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        for (int j = order_shoot1[i]; j < 10; j += 4)
                        {
                            if (!_shooted.Contains(new Point(j, i)))
                            {
                                toRet = new Point(j, i);
                                _shooted.Add(toRet);
                                turn = !turn;
                                return toRet;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 9; i >= 0; i--)
                    {
                        for (int j = order_shoot1[i]; j < 10; j += 4)
                        {
                            if (!_shooted.Contains(new Point(j, i)))
                            {
                                toRet = new Point(j, i);
                                _shooted.Add(toRet);
                                turn = !turn;
                                return toRet;
                            }
                        }
                    }
                }
                Random rand = new Random();
                Point toshoot = new Point(rand.Next(0, 10), rand.Next(0, 10));
                while (_shooted.Contains(toshoot))
                {
                    toshoot = new Point(rand.Next(0, 10), rand.Next(0, 10));
                }
                _shooted.Add(toshoot);
                return toshoot;
            }
            else
                return ShootHit(status);
        }
        public Point Shoot(int status = 0)
        {
            Point toRet = new Point();
            switch (status)
            {
                case 0:
                    return newShoot(status);
                    break;
                case 1:
                    return ShootHit(status);
                    break;
                case 2:
                    ShootHit(1);
                    foreach (var item in _hit)
                    {
                        if (item.X - 1 >= 0 && item.Y - 1 >= 0 && !_hit.Contains(new Point(item.X - 1, item.Y - 1)))
                        {
                            toRet = new Point(item.X - 1, item.Y - 1);
                            _shooted.Add(toRet);
                        }
                        if (item.X - 1 >= 0 && !_hit.Contains(new Point(item.X - 1, item.Y)))
                        {
                            toRet = new Point(item.X - 1, item.Y);
                            _shooted.Add(toRet);
                        }
                        if (item.X - 1 >= 0 && item.Y + 1 <= 9 && !_hit.Contains(new Point(item.X - 1, item.Y + 1)))
                        {
                            toRet = new Point(item.X - 1, item.Y + 1);
                            _shooted.Add(toRet);
                        }
                        if (item.Y + 1 <= 9 && !_hit.Contains(new Point(item.X, item.Y + 1)))
                        {
                            toRet = new Point(item.X, item.Y + 1);
                            _shooted.Add(toRet);
                        }
                        if (item.X + 1 <= 9 && item.Y + 1 <= 9 && !_hit.Contains(new Point(item.X + 1, item.Y + 1)))
                        {
                            toRet = new Point(item.X + 1, item.Y + 1);
                            _shooted.Add(toRet);
                        }
                        if (item.X + 1 <= 9 && !_hit.Contains(new Point(item.X + 1, item.Y)))
                        {
                            toRet = new Point(item.X + 1, item.Y);
                            _shooted.Add(toRet);
                        }
                        if (item.X + 1 <= 9 && item.Y - 1 >= 0 && !_hit.Contains(new Point(item.X + 1, item.Y - 1)))
                        {
                            toRet = new Point(item.X + 1, item.Y - 1);
                            _shooted.Add(toRet);
                        }
                        if (item.Y - 1 >= 0 && !_hit.Contains(new Point(item.X, item.Y - 1)))
                        {
                            toRet = new Point(item.X, item.Y - 1);
                            _shooted.Add(toRet);
                        }
                    }
                    _hit.Clear();
                    return newShoot(status);
                    break;
            }
            return new Point();

        }

    }
}
